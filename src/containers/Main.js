import React, { Component } from 'react';
import { Card } from 'antd';
import { Input } from 'antd';
import 'antd/dist/antd.css';
import datos from './../links.json'
const Search = Input.Search;

const gridStyle = {
    width: '200px',
    textAlign: 'center',
    height : "100px",
    backgroundColor : "#1b2836",
    color : "#929697",
    margin : "15px",
  };
  
class Main extends Component {
    state={
        links : false
    }

    async componentDidMount(){
  this.setState({links : datos});
    }

  render() {
    return (
      <div>
          {/*
          <div className="row" style={{paddingTop : "200px"}}>
            <div className="col-xs-12">
                <div className="box box-container">
                    <div className="row around-xs">
                        <div className="col-xs-2">
                            <div className="box-nested">

                            </div>
                        </div>
                        <div className="col-xs-6">
                            <div className="box-nested">

        <div style={{width : "100%",height : "20px"}}> 
        <span style={{color : "#929697",fontSize : "2em"}}> Search  </span>
        </div> 

    <br/>

     
                            </div>
                        </div>
                        <div className="col-xs-2">
                            <div className="box-nested">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          */}
           <div className="flexContainer flexCenter itemCenter fullHeight ">
                <div className="flexContainer flexCenter itemCenter fullHeight">
       
        <Card  bodyStyle={{backgroundColor : "#141b28",border: '5px solid #101521',color : "white"}} style={{}} 
        bordered={false} hoverable={true}
        >

        {this.state.links ? this.state.links.map((element,key)=>{
            console.log(element);
            return  <a href={element.link} key={key} style={{color : "inherit",textDecoration : "none"}}><Card.Grid style={gridStyle} ><i className={element.icon} style={{fontSize : "2em"}}/> 
            <p style={{marginTop : "5px"}}> {element.description} </p></Card.Grid></a>
 
        }): null}   
            </Card>
            </div>
            </div>
      </div>
    );
  }
}

export default Main;
